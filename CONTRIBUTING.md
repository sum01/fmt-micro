# Contributing

Everything below assumes you're using
[the Micro text-editor](https://github.com/zyedidia/micro) and this `fmt`
plugin.

**Tooling for contributions:**

- Use the [editorconfig](http://editorconfig.org/) plugin to keep everything
  uniform, which can be installed by doing `plugin install editorconfig`
- Download and use [LuaFormatter](https://github.com/Koihik/LuaFormatter) if you're editing a `.lua`
  file, which can be run with `fmt lua-format`
- Download and use [prettier](https://github.com/prettier/prettier) if you're editing a `.md`
  file, which can be run with `fmt prettier`

## Git workflow

First fork the repo, create a branch from `master`, push your changes to your repo, then submit a PR.

Please don't commit tons of changes in one big commit. Instead, use `git add -p` to selectively add lines.

When making changes, put a short blurb about it under `Unreleased` in the `CHANGELOG.md`.  
Make sure to adhear to the [Keep a changelog](https://keepachangelog.com/en/1.0.0/) format.

Changes to `.md` files can be left out of the changelog.

## Adding another formatter

Create a new formatter with the `new_formatter` function.

- The `cli` should be the literal command to run the formatter.
- `supports` should be from the `languages` table (that we created). If it supports multiple languages, add them all as a table.
- `args` are optional, but if needed, make sure that you use the proper types (and use `new_arg` to declare args): - `any` for arguments that are independent of if the editor is using tabs or spaces. - `tabs` for arguments that are specific to if the editor is using tabs. - `spaces` for arguments that are specific to if the editor is using spaces.

### Adding another language

To see if Micro supports the filetype, open the file and run `show filetype`.

If the language is supported by Micro, use its "proper" name (so `golang` instead of `go`) as an `alt_name` in the `new_language` declaration.

If that language is `Unknown`, you should use the "proper" name of the language, as well as adding all of its filetype extensions to the `extensions` table in your `new_language` declaration.
