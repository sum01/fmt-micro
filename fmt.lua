VERSION = "4.0.0"

-- Here we do all our imports since they're used throughout the code

-- Needed to access the info of the current pane/buffer (to get its filepath)
local micro = import("micro")
-- Used for writing info/error messages to the log (which can be brought up with the "log" command)
local micro_buffer = import("micro/buffer")
-- Used for finding the config dir to write config files
local micro_config = import("micro/config")
-- Used to get a filepath's extension
local golib_filepath = import("filepath")
-- Needed to get the current dir that micro is running in (usually your project dir)
local golib_os = import("os")
-- Used to scan the working dir for any conf files (when needed)
local golib_ioutil = import("io/ioutil")
-- Used to spawn the job that runs the formatter
local micro_shell = import("micro/shell")

-- These will hold important info about languages/formatters
-- Each index is constructed using some special functions
-- That can be seen below (after the get_filetype stuff)
local languages = {}
local formatters = {}

-- Accepts either a micro.CurPane() or literal filepath..
-- and returns either Micro's micro.CurPane():FileType(), or the extension string.
local function get_filetype(x)
	-- Uses Go's path.Ext() to get the literal file extension, as fallback
	local function get_gopath_ext(f_path)
		-- Use the path.Ext command to get the extension (with period), if it exists
		local f_type = golib_filepath.Ext(f_path)

		-- Returns an empty string if it doesn't find an extension
		if f_type == "" then
			-- Stop running if there's no extension
			return nil
		else
			-- Return the extension without the period from Go's path.Ext()
			return f_type:sub(2)
		end
	end

	local file_type = nil

	-- When passed a view, use the built-in micro.CurPane():FileType()
	-- micro.CurPane() is a "userdata" type, while a literal filepath would be a "string"
	if type(x) == "userdata" then
		file_type = x:FileType()
		-- Returns "Unknown" when Micro can't file the type
		if file_type == "Unknown" then
			-- Fallback to the literal extension
			file_type = get_gopath_ext(x.Path)
		end
	else
		-- When passed a direct path, use Go's path lib
		file_type = get_gopath_ext(x)
	end

	return file_type
end

-- Append on languages to our list of languages
local function new_language(params)
	assert(type(params) == "table",
       	"Error in languages.lua, bad paramas given to new_language()")
	-- This is the only required parameter
	-- We can handle anything else missing
	assert(params.name ~= nil and type(params.name) == "string")
	-- If no filetypes are given, we default to using the name as its filetype
	languages[params.name] = {
		-- Always the alt_name (if it exists)
		language = params.alt_name and params.alt_name or params.name,
		-- Always the raw filetype name (as it's known by Micro)
		file_type_name = params.name,
		extensions = (params.extensions or {params.name})
	}
end

-- Most of these language extensions are taken from Wikipedia

new_language({name = "crystal", extensions = {"cr"}})
new_language({name = "fish"})
new_language({name = "ruby"})
new_language({name = "go", alt_name = "golang"})
new_language({name = "javascript", extensions = {"js"}})
new_language({name = "jsx"})
new_language({name = "flow"})
new_language({name = "typescript", extensions = {"tx", "tsx"}})
new_language({name = "css"})
new_language({name = "less"})
new_language({name = "scss", extensions = {"sass", "scss"}})
new_language({name = "json"})
new_language({name = "graphql"})
new_language({name = "markdown", extensions = {"md", "markdown"}})
new_language({name = "rust", extensions = {"rs", "rlib"}})
new_language({
	name = "python",
	extensions = {"py", "pyc", "pyd", "pyo", "pyw", "pyz"}
})
new_language({
	name = "php",
	extensions = {"php", "phtml", "php3", "php4", "php5", "php7", "phps", "php-s"}
})
new_language({name = "c", extensions = {"c", "h"}})
new_language({
	name = "c++",
	-- We can't use ++ (easily) as a table's index, this makes it easier to access
	alt_name = "cpp",
	extensions = {"C", "cc", "cpp", "cxx", "c++", "h", "hh", "hpp", "hxx", "h++"}
})
new_language({name = "csharp", extensions = {"cs"}})
new_language({
	name = "objective-c",
	-- The dash can cause problems
	alt_name = "objectivec",
	extensions = {"h", "m", "mm", "M"}
})
new_language({name = "d", alt_name = "dlang", extensions = {"d"}})
new_language({name = "vala"})
new_language({name = "p", alt_name = "pawn"})
new_language({name = "java", extensions = {"java", "class", "jar"}})
new_language({name = "clojure", extensions = {"clj", "cljs", "cljc", "edn"}})
new_language({name = "perl", extensions = {"plx", "pl", "pm", "xs", "t", "pod"}})
new_language({name = "shell", alt_name = "bash"})
new_language({name = "html", extensions = {"html", "htm"}})
new_language({name = "xml"})
new_language({name = "lua"})

local function is_tabs()
	return micro_config.GetGlobalOption("tabstospaces") and false or true
end

local function get_indent_size()
	return micro_config.GetGlobalOption("tabsize")
end

-- Custom object to hold the eventual actual commands we run
-- Just holds our parsed output from the command string in the options
-- See http://lua-users.org/wiki/ObjectOrientationTutorial for what some of this does
local cli = {["cmd"] = nil, ["args"] = {}}
cli.__index = cli

setmetatable(cli, {
	__call = function(cls, ...)
		return cls.new(...)
	end
})

function cli:set_arg(new_arg)
	self.args[#self.args + 1] = new_arg
end
function cli:get_name()
	return self.cmd
end

-- Creates a new cli object to be used elsewhere
function cli.new()
	local self = setmetatable({}, cli)
	return self
end

-- Some shorthand functions to make code more readable
local function replace_with_config()
	return {replace_with_config = true}
end

local function replace_with_spaces()
	return {replace_with_spaces = true}
end

local function replace_with_tabs()
	return {replace_with_tabs = true}
end

-- The design of each tab/space arg, which self-identifies if it needs to be replaced
-- At runtime, we check the various "replace_with_xxx" settings and replace them as needed
local function new_arg(args)
	-- Lets us type less for things that don't use the member vars of "args"
	-- if type(args) == "string" then
	--	args.str = args
	-- end
	return {
		-- The individual arg, which can be empty for formatters that don't take them
		arg = args or "",
		-- This is used for things like "-i=3"
		-- where the following arg needs to not be spaced off of "-i="
		requires_suffix = args.suffix or false,
		-- Tells us if we should replace self.arg with stuff like..
		-- the result of is_tabs_str() or get_indent_size()
		-- or even to get a config file
		replace_with_config = args.replace_with_config or false,
		replace_with_tabs = args.replace_with_tabs or false,
		replace_with_spaces = args.replace_with_spaces or false
	}
end

-- Should probably be formatter.new() but I don't want this in the global scope
local function new_formatter(obj)
	local function force_table(x)
		if type(x) ~= "table" then
			-- Since sometimes no args are passed to new_formatter, return empty table
			if x == nil then
				return {}
			else
				return {x}
			end
		else
			return x
		end
	end

	-- Not all objects declare all of these, but we need to make sure they exist
	-- Otherwise when attempting to access it'll be an out-of-bounds err
	if not obj["args"] then
		obj["args"] = {}
	end
	if not obj.args["any"] then
		obj.args["any"] = {}
	end
	if not obj.args["tabs"] then
		obj.args["tabs"] = {}
	end
	if not obj.args["spaces"] then
		obj.args["spaces"] = {}
	end

	-- A bunch of "empty initializers" for less typed code
	obj.name = obj.cli
	-- Create an alt_name if we have one
	-- Otherwise copy the regular cli name
	obj.alt_name = obj.alt_name or obj.name
	-- Allow for formatters that can't use stdin to still be used
	-- obj.can_use_stdin = obj.can_use_stdin or true
	obj.supports = force_table(obj.supports)
	obj.args.any = force_table(obj.args.any)
	obj.args.tabs = force_table(obj.args.tabs)
	obj.args.spaces = force_table(obj.args.spaces)

	obj.args.get = function(self)
		-- Since this is called with obj.args:get(), it passes the object at obj.args
		-- so we don't have to type obj.args before everything...

		-- Remove the need to write the same code twice
		-- If the settings are saying we're tabs, we use this obj's tab args
		-- Otherwise we use its spaces args
		local tar_args = (is_tabs() and self.tabs or self.spaces)

		local full_args = {}

		-- Build the list of args, from either the tabs/spaces args, first
		for i = 1, #tar_args do
			full_args[#full_args + 1] = tar_args[i]
		end

		-- Put the "any" args after tabs/spaces, in case there's something that needs to be at the end
		for i = 1, #self.any do
			full_args[#full_args + 1] = self.any[i]
		end

		-- Return the raw arg table to be further manipulated in fmt.lua
		-- This will allow for appending runtime settings, such as indent size, onto a previous arg...
		-- such as "-i=INDENTSIZE"
		return full_args
	end

	-- Easy function to test for language support
	obj.does_support = function(self, tar_lang)
		return self.supports[tar_lang]
	end

	-- Create a pre-formatted cli obj to use
	obj.get_cli_obj = function(self)
		local ret_cli = cli.new()
		ret_cli.cmd = self.cmd
		ret_cli.args = self.args:get()
		return ret_cli
	end

	-- Use as a dict to allow for easy lookup on command calls & whatnot
	-- But use the "pretty" .name in case the .cli is some ugly call
	-- This means we'll expect "fmt formattername" to reference the obj.name
	formatters[obj.name] = obj
end

-- ~~ Start of the actual default formatters ~~

new_formatter({
	cli = "crystal",
	supports = languages["crystal"],
	args = {any = new_arg("tool format")}
})

new_formatter({
	cli = "fish_indent",
	supports = languages["fish"],
	args = {any = new_arg("-w")}
})

-- No args
new_formatter({cli = "rufo", supports = languages["ruby"]})

new_formatter({
	cli = "rubocop",
	supports = languages["ruby"],
	args = {
		-- Since nothing is special, declare it all as once arg
		any = new_arg("-f quiet -o -s")
	}
})

new_formatter({
	cli = "gofmt",
	supports = languages["go"],
	args = {any = new_arg("-s")}
})

new_formatter({cli = "goimports", supports = languages["go"]})

new_formatter({
	cli = "prettier",
	supports = {
		languages["javascript"],
		languages["jsx"],
		languages["flow"],
		languages["typescript"],
		languages["css"],
		languages["less"],
		languages["scss"],
		languages["json"],
		languages["graphql"],
		languages["markdown"],
		languages["html"]
	},
	args = {
		tabs = {
			new_arg("--use-tabs"),
			new_arg("true"),
			new_arg("--tab-width"),
			new_arg(replace_with_tabs()),
			new_arg("--write")
		},
		spaces = {
			new_arg("--use-tabs"),
			new_arg("false"),
			new_arg("--tab-width"),
			new_arg(replace_with_spaces()),
			new_arg("--write")
		}
	}
})

new_formatter({cli = "rustfmt", supports = languages["rust"]})

new_formatter({cli = "yapf", supports = languages["python"]})

new_formatter({cli = "black", supports = languages["python"]})

new_formatter({
	cli = "php-cs-fixer",
	supports = languages["php"],
	args = {any = new_arg("fix")}
})

new_formatter({
	cli = "uncrustify",
	supports = {
		languages["c"],
		languages["cpp"],
		languages["csharp"],
		languages["objectivec"],
		languages["d"],
		languages["java"],
		languages["pawn"],
		languages["vala"]
	},
	args = {
		any = {new_arg("-c"), new_arg(replace_with_config()), new_arg("--no-backup")}
	}
})

new_formatter({cli = "cljfmt", supports = languages["clojure"]})

new_formatter({
	cli = "elm-format",
	supports = languages["elm"],
	args = {any = new_arg("--yes")}
})

new_formatter({
	cli = "clang-format",
	supports = {languages["c"], languages["cpp"], languages["objectivec"]}
})

new_formatter({cli = "latexindent.pl", supports = languages["latex"]})

new_formatter({
	cli = "csscomb",
	supports = languages["css"],
	args = {any = new_arg("-t")}
})

new_formatter({cli = "marko-prettyprint", supports = languages["marko"]})

new_formatter({cli = "ocp-indent", supports = languages["ocaml"]})

new_formatter({
	cli = "align",
	supports = languages["yaml"],
	args = {
		-- This will go after because of new_formatter() logic
		any = new_arg("-s"),
		spaces = {new_arg("-p"), new_arg(replace_with_spaces())},
		tabs = {new_arg("-p"), new_arg(replace_with_tabs())}
	}
})

new_formatter({cli = "stylish-haskell", supports = languages["haskell"]})

new_formatter({
	cli = "puppet-lint",
	supports = languages["puppet"],
	args = {any = new_arg("--fix")}
})

new_formatter({
	cli = "autopep8",
	supports = languages["python"],
	args = {
		-- The -a arg can be used multiple times to increase aggresiveness. Unsure of what people prefer, so doing 1.
		any = new_arg("-a -i")
	}
})

new_formatter({
	cli = "tsfmt",
	supports = languages["typescript"],
	args = {any = new_arg("-r")}
})

new_formatter({cli = "dartfmt", supports = languages["dart"]})

new_formatter({
	cli = "fprettify",
	supports = languages["fortran"],
	args = {
		any = new_arg("--silent"),
		spaces = {new_arg("--indent"), new_arg(replace_with_spaces())},
		tabs = {new_arg("--indent"), new_arg(replace_with_tabs())}
	}
})

new_formatter({
	cli = "htmlbeautifier",
	supports = {languages["html"], languages["ruby"]},
	args = {
		spaces = new_arg("-T"),
		tabs = {new_arg("-t"), new_arg(replace_with_tabs())}
	}
})

new_formatter({
	cli = "coffee-fmt",
	supports = languages["coffee"],
	args = {spaces = new_arg("space"), tabs = new_arg("tab")}
})

new_formatter({
	cli = "pug-beautifier",
	supports = languages["pug"],
	args = {tabs = {new_arg("-t"), new_arg(replace_with_tabs())}}
})

new_formatter({
	cli = "perltidy",
	supports = languages["perl"],
	args = {
		tabs = {new_arg("-et="), new_arg(replace_with_tabs())},
		spaces = {new_arg("-i="), new_arg(replace_with_spaces())}
	}
})

new_formatter({
	cli = "js-beautify",
	supports = languages["javascript"],
	args = {
		tabs = new_arg("-t"),
		spaces = {new_arg("-s"), new_arg(replace_with_spaces())}
	}
})

new_formatter({
	cli = "shfmt",
	supports = languages["shell"],
	args = {tabs = new_arg("0"), spaces = new_arg(replace_with_spaces())}
})

new_formatter({
	cli = "beautysh",
	supports = languages["shell"],
	args = {
		tabs = {new_arg("-t")},
		spaces = {new_arg("-i"), new_arg(replace_with_spaces())}
	}
})

new_formatter({
	cli = "dfmt",
	supports = languages["d"],
	args = {
		tabs = {new_arg("tab"), new_arg("--tab_width")},
		spaces = {new_arg("space"), new_arg("--indent_size")}
	}
})

new_formatter({
	cli = "tidy",
	supports = {languages["html"], languages["xml"]},
	args = {tabs = new_arg("0"), spaces = new_arg(replace_with_spaces())}
})

new_formatter({
	cli = "luafmt",
	supports = languages["lua"],
	args = {
		tabs = {new_arg(replace_with_tabs()), new_arg("--use-tabs")},
		spaces = new_arg(replace_with_spaces())
	}
})

new_formatter({
	cli = "lua-format",
	supports = languages["lua"],
	args = {
		any = {new_arg("-c"), new_arg(replace_with_config()), new_arg("--no-backup")}
	}
})

-- Our own special json file for saving settings
-- This helps hide the mess of own plugin from micro's own settings
-- local fmt_confpath = golib_filepath.Join(micro_config.ConfigDir, "fmt_settings.json")

-- Returns a full path to either a config file in the directory, or our bundled one.
-- extension should be a string of the file extension needed, sans period.
-- name is the folder name in our bundled configs | ex: fmt-micro/configs/NAME
local function get_conf(name, extension)
	-- The current local dir
	local dir = golib_os.Getwd()
	-- Gets an array of all the files in the current dir
	local readout = golib_ioutil.ReadDir(dir)

	if readout ~= nil then
		-- The full path to the file
		local readout_path

		for i = 1, #readout do
			-- Save the current file's full path
			readout_path = golib_filepath.Join(dir, readout[i]:Name())

			-- if extension matches, return path to the config file
			if get_filetype(readout_path) == extension then
				micro_buffer.Log("fmt: Found " .. name .. "'s config, using \"" ..
                 					readout_path .. "\"")
				-- Return the found local config
				return readout_path
			end
		end
	end

	-- Fallback onto our bundled config if no local one is found
	local bundled_conf = golib_filepath.Join(micro_config.ConfigDir, "plugins",
                                         	"fmt", "configs", name)
	micro_buffer.Log("fmt: Didn't find " .. name .. "'s config, using bundled \"",
                 	bundled_conf .. "\"")
	-- Return the bundled config path for the requested config
	return bundled_conf
end

function onStdout(out)
	if out ~= "" then
		-- Log any std output from formatting to Micro's log
		micro_buffer.Log("fmt info: " .. out)
	end
end

function onExit()
	-- Refresh the current open file after the format command finishes
	micro.CurPane().Buf:ReOpen()
end

function onStderr(err)
	if err ~= "" then
		-- Log any std err from formatting to Micro's log
		micro_buffer.Log("fmt error: " .. err)
	end
end

-- Actually runs the formatting job on the file
local function run_formatter_job(cli_obj)
	-- Put the filepath as the last arg for the command we'll run
	cli_obj:set_arg(micro.CurPane().Buf.AbsPath)

	-- Log exactly what will run and on what file
	-- The table.concat just puts spaces between the args for easier reading
	micro_buffer.Log("fmt: Running \"" .. cli_obj.cmd .. " " ..
                 		table.concat(cli_obj.args, " ") .. "\"\n")

	-- Actually run the command with Micro's binding to the Go exec.Command()
	micro.CurPane().Buf.ReloadDisabled = true
	micro_shell.JobSpawn(cli_obj.cmd, cli_obj.args, fmt.onStdout, fmt.onStderr,
                     	fmt.onExit)
end

-- Parses the raw string option from Micro's config. Returns a cli object containing the cmd & args.
-- Optionally you can pass a "true" for the second arg to immediately return just the command...
-- which will then not parse the rest of the option.
local function parse_formatter_option(formatter_option)
	-- Create a new instance of the cli object to use
	local parsed_cli = cli.new()
	-- Keep track of the current string matched
	local is_first = true
	-- Have to use regex matching to find their custom command
	-- Goes through each word match
	for match in formatter_option:gmatch("%S+") do
		-- Save the first match as the actual command name
		if is_first then
			parsed_cli.cmd = match
			is_first = false
		else
			-- parsed_cli[1] will be the command, the rest will be the args
			parsed_cli:set_arg(match)
		end
	end
	-- Return our formatted parsed_cli object
	return parsed_cli
end

local function find_known_formatter(cli_obj)
	-- FIXME the cli object here seems to not be formed properly
	-- it's bugging when trying to call get_name()
	-- so maybe malformed in parse_formatter_option or the metatable command cli.new() is incorrect
	return formatters[cli_obj:get_name()]
end

-- Find the correct formatter, its arguments, and then run on the current file
local function format()
	-- Prevent infinite loop of onSave()
	-- micro.CurPane():Save(false)

	-- Get the current file type so we can see if we support it
	local file_type = get_filetype(micro.CurPane().Buf)
	-- Stop running if not a known extension/file type
	if file_type == nil then
		-- Log the err so the user knows why it didn't work
		micro_buffer.Log("fmt: Unidentified file type, aborting formatting.")
		return
	end

	-- Check if there's a known language for this file type
	-- Note that the languages indices are always the literal filetype reported by Micro
	local found_known_lang = languages[file_type]
	if found_known_lang == nil then
		micro_buffer.Log("fmt: Unrecognized file type, aborting formatting.")
		return
	end

	-- Check if there's a saved formatter setting for this language
	-- This looks for an option like "fmt.cpp": "uncrustify"
	local formatter_in_options = micro_config.GetGlobalOption(
                             		"fmt." .. found_known_lang.language)
	-- This looks for an option like "fmt.c++": "uncrustify"
	local formatter_in_options_typename = micro_config.GetGlobalOption(
                                      		"fmt." ..
                                      			found_known_lang.file_type_name)

	if formatter_in_options == nil then
		-- We check typename since they might've set the literal filetype as a setting
		if formatter_in_options_typename == nil then
			-- Log the err so the user knows why it didn't work
			-- found_known_lang.language will either be the alt_name or the literal name (prefers alt_name if available)
			micro_buffer.Log("fmt: No formatter found in your config for the \"" ..
                 				found_known_lang.language ..
                 				"\" language, aborting formatting.")
			return
		else
			-- If we found the literal fieltype representation but not the "fancy" alt name version..
			-- we can just overwrite for easy use in the following code.
			formatter_in_options = formatter_in_options_typename
		end
	end

	-- Parse the option we found for use
	-- Basically reads the literal string from settings.json into an easy-to-use object
	-- Note that if we found two different options for the "alt_name" and literal filetype name
	-- then we'll just prefer the alt_name option, since we need to pick one.
	local parsed_formatter_obj = parse_formatter_option(formatter_in_options)

	local found_known_formatter = find_known_formatter(parsed_formatter_obj)
	-- If they have a saved formatter, we use that
	-- Note we check for empty args in case they defined different settings for a known formatter
	if found_known_formatter ~= nil and parsed_formatter_obj.args == nil and
		parsed_formatter_obj:does_support(file_type) then
		-- Feed the parsed cli object to our formatter job
		-- We only use the command because it's how the formatters object saves names
		run_formatter_job(found_known_formatter:get_cli_obj())
	else
		-- Since we don't know this formatter (or its a known with different args), we create a custom object to use it..
		-- then feed it to the formatter job runner
		run_formatter_job(parsed_formatter_obj)
	end
end

function onSave(view)
	-- Allows for enable/disable on-save formatting via the option
	if micro_config.GetGlobalOption("fmt.onsave") then
		format()
	end
end

-- init() is needed for Micro (v2) to recognize/start the plugin
function init()
	-- Default onsave formatter as enabled
	micro_config.RegisterCommonOption("fmt", "onsave", true)
	-- User command to run format()
	-- Note we don't want autocomplete on this, since Micro doesn't know what it could be
	micro_config.MakeCommand("fmt", format, micro_config.NoComplete)
	-- The help file the user can read
	micro_config.AddRuntimeFile("fmt", micro_config.RTHelp, "help/fmt.md")
end
