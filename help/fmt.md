# fmt plugin

To manually run formatting on the current file, use the `fmt` command.

To manually run a specific formatter on the current file, use the `fmt formattername` command.

When saving a supported file-type, the plugin will automatically run on the file
& save any changes, unless you `set fmt.onsave false`.

To see the list of supported languages/formatters, see the README.md for the full list.  
The README can be found in ~/.config/micro/plug/fmt-micro or at https://gitlab.com/sum01/fmt-micro/-/blob/master/README.md

Please note that formatting of all languages is disabled by default, as to not accidentally
format your files. You must enable them individually for this plugin to do
anything.

## What's Bundled?

No formatters are bundled with this plugin. You must install the formatter you
want or it won't work.

Some config files are bundled with this plugin, but are only used when one can't
be found in your current working directory.

## Config Files

If you added a config file and want to update settings, run `fmt update` to
force settings to refresh.

The fall-back paths to the bundled config files don't have hard-coded names, so
you can delete/edit the one in the relevant folder, and it should still work.

#### Using Custom Formatter/Args

You can add your own formatter, or just use different args, by adding its command (and file type) into `settings.json`  
The format looks like `"fmt.js": "prettier --write"` in your `settings.json`.

Note that you should use the file type detected by Micro.  
You can check a file's type by running `show filetype`
